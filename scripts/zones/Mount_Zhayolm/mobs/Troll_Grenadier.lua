-----------------------------------
--   Area: Mount Zhayolm
--    Mob: Troll Grenadier
-- Author: Spaceballs
--   Note: Pet of Khromasoul Bhurborlor
-----------------------------------


-- RDM Mobs immune to sleep

mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/status")


function onMobDespawn(mob)
end

function onMobDeath(mob, player, isKiller)
end